'use strict';

/*
 Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

    - робота з не перевіренними даними. Якщо вхідний масив з 1000 об'ектів має помилку у 2 об'екті, завантаження даних зупиниться. необхідно все завантажити та відокремити помилки;
    - перевірка окремої ділянки великого коду на помилки;
    - обробка конкретного виду помилки;
    - обробка винятків під час роботи з файлами та збою мережі;

*/

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const div = document.createElement('div');
div.id = 'root';

class BookList {
    constructor(books) {
        this.books = books;
    }

    createList() {
        const ul = document.createElement("ul");

        this.books.forEach((arr, index) => {
            try {
                if (arr.author === undefined) {
                    throw new Error(`У записі №${index} відсутній автор!`);
                }
                if (arr.name === undefined) {
                    throw new Error(`У записі №${index} відсутня назва!`);
                }
                if (arr.price === undefined) {
                    throw new Error(`У записі №${index} відсутня ціна!`);
                }

                const li = document.createElement("li");
                li.textContent = `${arr.author} "${arr.name}" - ціна ${arr.price} грн.`;
                ul.appendChild(li);

            } catch (error) {
                console.error(error);
            }
        });

        return ul;
    }
}

const bookList = new BookList(books);
const ul = bookList.createList();
document.body.appendChild(div).append(ul);
